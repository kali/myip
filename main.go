package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"strconv"

	"github.com/ip2location/ip2location-go"
	"github.com/julienschmidt/httprouter"
)

func getIP(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	ip, _, err := net.SplitHostPort(req.RemoteAddr)
	if err != nil {
		fmt.Fprintf(w, "userip: %q is not IP:port", req.RemoteAddr)
	}

	userIP := net.ParseIP(ip)
	if userIP == nil {
		fmt.Fprintf(w, "userip: %q is not IP:port", req.RemoteAddr)
		return
	}

	forward := req.Header.Get("X-Forwarded-For")

	fmt.Fprintf(w, "<p>YourFuckingIP: %s</p>", ip)
	if forward != "" {
		fmt.Fprintf(w, "<p>YourFuckingForwardedFor: %s</p>", forward)
	}
}

func getIPJSON(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	ip2location.Open("IP2LOCATION-LITE-DB3.BIN")

	ip, _, err := net.SplitHostPort(req.RemoteAddr)
	if err != nil {
		log.Fatal(err)
	}
	netIP := net.ParseIP(ip)

	results := ip2location.Get_all(netIP.String())

	data := map[string]string{
		"ip":      netIP.String(),
		"cc":      results.Country_short,
		"country": results.Country_long,
		"city":    results.City,
	}
	dataJSON, _ := json.Marshal(data)
	fmt.Fprintf(w, string(dataJSON))
}

func main() {
	myport := strconv.Itoa(9000)

	r := httprouter.New()
	r.GET("/", getIP)
	r.GET("/json", getIPJSON)

	l, err := net.Listen("tcp", "0.0.0.0:"+myport)
	if err != nil {
		log.Fatal(err)
	}
	log.Fatal(http.Serve(l, r))
}
